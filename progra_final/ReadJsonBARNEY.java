/*
/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import java.io.File;
import java.io.FileReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class ReadJsonBARNEY {

    private int hora_hambre;
    private int hora_feliz;
    private int hora_banno;
    private int hora_salud;
    public ReadJsonBARNEY() {
    }
 /*-----------------------------------------------------------------------
	read
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Lee un Json
-----------------------------------------------------------------------*/
    public void read(){
    
        ClassLoader classLoader= new ReadJsonDOGGE().getClass().getClassLoader();
        String fileName="tamaguchi/BARNEY.json";
        File file = new File(classLoader.getResource(fileName).getFile());
        
        JSONParser parser = new JSONParser();
        try{
        
            FileReader reader = new FileReader(file.getAbsolutePath());
            Object obj = parser.parse(reader);
            JSONObject jsonObj =  (JSONObject)obj;
            JSONObject detalles = (JSONObject)jsonObj.get("detalles");
           Integer  hora_hambre= (int)detalles.get("hora_hambre");
           Integer hora_banno= (int)detalles.get("hora_banno");
           Integer hora_feliz= (int)detalles.get("hora_feliz");
           Integer hora_salud= (int)detalles.get("hora_salud");
            
        
        }catch(Exception e){
        
        
        }
    
    }

    public int getHora_hambre() {
        return hora_hambre;
    }

    public int getHora_feliz() {
        return hora_feliz;
    }

    public int getHora_banno() {
        return hora_banno;
    }

    public int getHora_salud() {
        return hora_salud;
    }


    
}
