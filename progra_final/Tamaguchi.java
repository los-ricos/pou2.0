/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author rzumb
 */
public class Tamaguchi extends Application {
    
 /*-----------------------------------------------------------------------
	start
        Entradas: Stage
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Soobreescribe el metodo start
-----------------------------------------------------------------------*/
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDogge.fxml"));
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("TAMAGUCHIN");
        stage.show();
         
      
    }

    //Main
    public static void main(String[] args) {
        
        launch(args);
        
        
    }
    
}
