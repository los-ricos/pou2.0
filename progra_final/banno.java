/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

 /*-----------------------------------------------------------------------
	banno
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Hilo banno
-----------------------------------------------------------------------*/
public class banno extends Task<Object>{
      
    private int hora_reloj=0;
    private boolean hora_atraso=false;
    private int atraso=2;
    private int hora_banno;
    private Button boton;
    private int subir=0;
    
    //Constructor banno
    public banno(int hora_banno, Button boton) {
        this.hora_banno = hora_banno;
        this.boton=boton;
    }
 
    public int mascota_banno(){
        int result;
        boton.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){

                subir=0;
                updateProgress(subir,10);
                atraso=2;
               hora_atraso=false; 
               
               

            }
        });
        if(subir>=10){
            subir=-20;
            updateProgress(subir,10);
            
        
        }
       if(subir<=0){
           subir=0;
           updateProgress(subir,10);
       
       }
        if(hora_reloj==0){
            result=subir=0;
            updateProgress(subir,10);
            
            return result;
        }
        
        if(hora_reloj==hora_banno){

            updateProgress(subir,10);
            
            
            result=subir=subir+2;
            updateProgress(subir,10);
            hora_atraso=true;
           
            return result;
        }
        
        if(hora_reloj==hora_banno+atraso && hora_atraso==true){

            result=subir=subir+2;
            updateProgress(subir,10);
            atraso=atraso+2;
            
            return result;
        }
        if(hora_reloj == 24){
        
            hora_reloj=0;
            atraso=2;
       
        }
        result=subir;
        return result;
    }


    @Override
    protected Object call() throws Exception {
        while(true){
            
            Thread.sleep(5000);
            mascota_banno();
            hora_reloj++;

        
        
        }
       
        
    }
}
