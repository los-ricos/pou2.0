/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import javafx.concurrent.Task;
 /*-----------------------------------------------------------------------
	puntaje
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Hilo puntaje
-----------------------------------------------------------------------*/
public class puntaje extends Task<Void>{
    private int num_felicidad;
    private int num_salud;
    private int num_suciedad;
    private int num_hambre;
    private int puntaje=0;
    
    //Constructor puntaje
    public puntaje(int num_felicidad, int num_salud, int num_suciedad, int num_hambre) {
        this.num_felicidad = num_felicidad;
        this.num_salud = num_salud;
        this.num_suciedad = num_suciedad;
        this.num_hambre = num_hambre;
    }
    
    public void leer(){
        if(num_felicidad > 6){
            puntaje=puntaje+10;
            updateMessage("Puntaje: "+puntaje);
        }
        if(num_felicidad <= 6){
            puntaje=puntaje-10;
            updateMessage("Puntaje: "+puntaje);
        }
        if(num_salud == 10){
            puntaje=puntaje+10;
            updateMessage("Puntaje: "+puntaje);
        }
        
        if(num_salud >= 4 && num_salud > 0){
            puntaje=puntaje-10;
            updateMessage("Puntaje: "+puntaje);
        }
        if(num_suciedad >= 8){
            puntaje=puntaje-10;
            updateMessage("Puntaje: "+puntaje);
        }
        if(num_hambre < 8){
            puntaje=puntaje+10;
            updateMessage("Puntaje: "+puntaje);
        }
    }
    
    @Override
    protected Void call() throws Exception {
       while(true){
           Thread.sleep(5000);
           leer();
       
       
       } 
    }
    
}
