/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import javafx.concurrent.Task;

    
 /*-----------------------------------------------------------------------
	time
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Hilo time
-----------------------------------------------------------------------*/
public class time extends Task<Integer> {
    
    private int hora;
    
  
    @Override
    protected Integer call() throws Exception {
        while(true){
                            
            Thread.sleep(5000);
            hora++;
            updateMessage("Hora\n"+hora+":00 hrs");
            
            if(hora==24){
                hora=0;
                
            }
            
            
                            
                            
        }
                        
                        
    }    
 
}
