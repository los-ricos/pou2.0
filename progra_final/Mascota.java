/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import javafx.scene.AccessibleAttribute;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

public class Mascota {
    private Button boton_alimentar;
    private Button boton_acariciar;
    private Button boton_limpiar;
    private Button boton_curar;
    private Label label_hora;
    private Label label_dia;
    private ProgressBar barra_hambre;
    private ProgressBar barra_suciedad;
    private ProgressBar barra_salud;
    private ProgressBar barra_felicidad;
    private int hora_hambre;
    private int hora_banno;
    private int hora_amor;
    private int hora_salud;
    private Label puntaje;
    
    //Constructor de Mascota
    public Mascota(Button boton_alimentar, Button boton_acariciar, Button boton_limpiar, Button boton_curar, Label label_hora, Label label_dia, ProgressBar barra_hambre, ProgressBar barra_suciedad, ProgressBar barra_salud, ProgressBar barra_felicidad, int hora_hambre, int hora_banno, int hora_amor, int hora_salud,Label puntaje) {
        this.boton_alimentar = boton_alimentar;
        this.boton_acariciar = boton_acariciar;
        this.boton_limpiar = boton_limpiar;
        this.boton_curar = boton_curar;
        this.label_hora = label_hora;
        this.label_dia = label_dia;
        this.barra_hambre = barra_hambre;
        this.barra_suciedad = barra_suciedad;
        this.barra_salud = barra_salud;
        this.barra_felicidad = barra_felicidad;
        this.hora_hambre = hora_hambre;
        this.hora_banno = hora_banno;
        this.hora_amor = hora_amor;
        this.hora_salud = hora_salud;
        this.puntaje=puntaje;
    }



        
 /*-----------------------------------------------------------------------
	iniciar_todo
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Comienza la ejecución
-----------------------------------------------------------------------*/

    public void iniciar_todo(){
        time task_time= new time();
        day task_day = new day();
        hambre task_comer = new hambre(4,boton_alimentar);
        banno task_banno = new banno(5,boton_limpiar);
        salud task_salud = new salud(hora_banno,hora_hambre,hora_salud,boton_limpiar,boton_curar);
        amor task_feliz=new amor(hora_amor,boton_limpiar,boton_acariciar);
        puntaje puntos = new puntaje(task_feliz.mascota_amor(),task_salud.mascota_salud(),task_banno.mascota_banno(),task_comer.mascota_hambre());
        

        label_hora.textProperty().unbind();
        label_hora.textProperty().bind(task_time.messageProperty());
       
        label_dia.textProperty().unbind();
        label_dia.textProperty().bind(task_day.messageProperty());
        
        barra_hambre.progressProperty().unbind();
        barra_hambre.progressProperty().bind(task_comer.progressProperty());

        
        barra_suciedad.progressProperty().unbind();
        barra_suciedad.progressProperty().bind(task_banno.progressProperty());

        barra_salud.progressProperty().unbind();
        barra_salud.progressProperty().bind(task_salud.progressProperty());
        
        barra_felicidad.progressProperty().unbind();
        barra_felicidad.progressProperty().bind(task_feliz.progressProperty());
  

        puntaje.textProperty().unbind();
        puntaje.textProperty().bind(puntos.messageProperty());
        
        new Thread(task_feliz).start();
        new Thread(task_banno).start();
        new Thread(task_salud).start();
        new Thread(task_comer).start();
        new Thread (task_time).start();
        new Thread(task_day).start();     
        new Thread(puntos).start();
        
        

    
    }

}
