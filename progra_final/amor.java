/*
Tecnologico de Costa Rica
Programacion Orientada a Objetos
Segundo semestre 2019
Estudiantes:
    Jeremy Madrigal Portilla
    Marco Madrigal Perez
    Randall Zumbado Huertas
Profesora:
    Samanta Ramijan Carmiol
*/

package tamaguchi;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

 /*-----------------------------------------------------------------------
	amor
        Entradas: No tiene
    Salidas: No posee
    Restricciones: No tiene
	Funcionamiento: 
        - Hilo amor
-----------------------------------------------------------------------*/
public class amor extends Task<Object> {
      
    private boolean hora_atraso=false;
    private int atraso=2;
    private int hora_reloj=0;
    private int hora_feliz;
    private Button boton_limpio;
    private Button boton_acariciar;
    private int subir=10;

    //Constructor amor
    public amor(int hora_feliz, Button boton_limpio,Button boton_acariciar) {
        this.hora_feliz = hora_feliz;
        this.boton_limpio=boton_limpio;
        this.boton_acariciar=boton_acariciar;

    }
 
    public int mascota_amor(){
        int result;
        boton_acariciar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                subir=10;
                updateProgress(subir,10);
                
                
                hora_atraso=false;
                
                atraso=2;
            }
        });
        
        boton_limpio.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                if(subir>=10){
                subir=10;
                updateProgress(subir,10);
                }
                else{
                subir=subir+2;
                updateProgress(subir,10);
            }

            }
        });
       
        if(hora_reloj==0){
            result=subir=10;
            updateProgress(subir,10);
            
        }
        
        if(hora_reloj==hora_feliz){
            if(subir<=0){
            result=subir=0;
            updateProgress(subir,10);
            
            return result;
            }
            else{
            result=subir=subir-2;
            updateProgress(subir,10);
            hora_atraso=true;
            
            return result;
            }
          
        }
        
        if(hora_reloj==hora_feliz+atraso && hora_atraso==true){
            if(subir<=0){
            result=subir=0;
            updateProgress(subir,10);
            
            return result;
            }
            else{
            result=subir=subir-2;
            updateProgress(subir,10);
            atraso=atraso+2;
            
            return result;
            }
        }
            result=subir;
           return result;
    
    }

    @Override
    protected Object call() throws Exception {
        while(true){
            mascota_amor();
            Thread.sleep(5000);
            hora_reloj++;
            
        
        
        
        }
    }
}
