/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tamaguchi;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author rzumb
 */
public class salud extends Task<Object> {
 private int hora_reloj=0;
    private boolean hora_atraso=false;
    private int atraso=2;
    private int hora_salud;
    private int hora_comer;
    private int hora_limpiar;
    private Button boton_limpio;
    private Button boton_curar;
    private int subir=10;
    private int bajar=2;
   
    

    public salud(int hora_limpiar,int hora_comer,int hora_salud, Button boton_limpio,Button boton_curar) {
        this.hora_salud = hora_salud;
        this.boton_limpio=boton_limpio;
        this.boton_curar=boton_curar;
        this.hora_comer=hora_comer;
        this.hora_limpiar=hora_limpiar;
        
    }
 
    public int mascota_salud(){
        int result;
        boton_curar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                subir=10;
                updateProgress(subir,10);
                
                
                hora_atraso=false;
                
                atraso=2;
            }
        });
        
        boton_limpio.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){

                if(subir>=10){
                subir=10;
                updateProgress(subir,10);
          
                }
                else{
                subir=subir+2;
                updateProgress(subir,10);
               
               
               }

            }
        });
       

        if(hora_reloj==0){
            result=subir=10;
            
            updateProgress(subir,10);
            
            
        }
        
        if(hora_reloj==hora_salud){
            if(subir<=0){
            result=subir=0;
            result=subir;
            updateProgress(subir,10);
            
            return result;
            }
            else{
            result=subir=subir-2;
            result=subir;
            updateProgress(subir,10);
            
            return result;
            }
          
        }
        
        if(hora_reloj==hora_salud+atraso && hora_atraso==true){
            if(subir<=0){
            result=subir=0;
            updateProgress(subir,10);
            
            return result;
            }
            else{
            result=subir=subir-2;
            updateProgress(subir,10);
            atraso=atraso+2;
            
            return result;
            }
        }
        
        if(hora_reloj==hora_comer || hora_reloj==hora_limpiar){
            if(subir<=0){
            result=subir=0;
            updateProgress(subir,10);
            
            return result;
            }
            else{
             result=subir=subir-bajar;
            updateProgress(subir,10);       
            
            return result;
            }
        }
        
        if(hora_reloj == 24){
        
            hora_reloj=0;
            atraso=2;
        }
        result=subir;
        return result;
        
    
    
    }

    @Override
    protected Object call() throws Exception {
        while(true){
            mascota_salud();
            Thread.sleep(5000);            
            hora_reloj++;
        
        
        
        }
    }
}
