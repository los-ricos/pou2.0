package tamaguchi;

import com.gluonhq.charm.glisten.control.ExpansionPanel.ExpandedPanel;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Controller_dogge implements Initializable{

    @FXML
    private VBox BOX;

    @FXML
    private AnchorPane panel;

    @FXML
    private ExpandedPanel panel_extendido;

    @FXML
    private MenuButton menu_mascotas;

    @FXML
    private MenuItem item_menu1;

    @FXML
    private MenuItem item_menu2;

    @FXML
    private Label label_felicidad;

    @FXML
    private ProgressBar barra_felicidad;

    @FXML
    private Label label_salud;

    @FXML
    private ProgressBar barra_salud;

    @FXML
    private Label label_suciedad;

    @FXML
    private ProgressBar barra_suciedad;

    @FXML
    private Label label_hambre;

    @FXML
    private ProgressBar barra_hambre;

    @FXML
    private Button boton_curar;

    @FXML
    private ImageView imagen_curar;

    @FXML
    private Button boton_limpiar;

    @FXML
    private ImageView imagen_limpiar;

    @FXML
    private Button boton_acariciar;

    @FXML
    private ImageView imagen_acariciar;

    @FXML
    private Button boton_alimentar;

    @FXML
    private ImageView imagen_alimentar;

    @FXML
    private Separator separador_vertical;

    @FXML
    private Separator separador_horizontal;

    @FXML
    private Label label_dia;

    @FXML
    private Label label_hora;

    @FXML
    private Label label_sentimiento;

    @FXML
    private Label label_mascota;

    @FXML
    private ImageView imagen_dogge;

    @FXML
    private Label Nombre;

    @FXML
    private Label Puntaje;

    @FXML
    private TextField nombre;
    
    private String dia; 
    
    private int indice=0;
    private int hora=1;
    
    
   
    
    @FXML
    public void comenzar_juego(ActionEvent event){

        nombre.setEditable(false);
        nombre.setDisable(true);
        ReadJsonDOGGE readD= new ReadJsonDOGGE();
        readD.read();
        
        Mascota dogge= new Mascota(boton_alimentar,boton_acariciar,boton_limpiar,boton_curar,label_hora,label_dia,barra_hambre,barra_suciedad,barra_salud,barra_felicidad,readD.getHora_hambre(),readD.getHora_banno(),readD.getHora_feliz(),readD.getHora_salud(),Puntaje);
                Stage stage1=new Stage();
                Parent root = null;
                try {
                    root = FXMLLoader.load(getClass().getResource("FXMLBarney.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(Controller_barney.class.getName()).log(Level.SEVERE, null, ex);
                }
        
                Scene scene = new Scene(root);
        
                stage1.setScene(scene);
                stage1.setTitle("TAMAGUCHIN");
                
        item_menu2.setOnAction(new EventHandler<ActionEvent>(){
            
            @Override
            public void handle(ActionEvent event) {
                item_menu2.setDisable(true);
                stage1.show();
            }
        });
        Mascota doggee= new Mascota(boton_alimentar,boton_acariciar,boton_limpiar,boton_curar,label_hora,label_dia,barra_hambre,barra_suciedad,barra_salud,barra_felicidad,4,2,6,13,Puntaje);
        doggee.iniciar_todo();

      
       
    }
  
    
    @FXML
    public void mensaje_acariciar(ActionEvent event) {
   

    }


    @FXML
    public void mensaje_curar(ActionEvent event) {


    }
   


    @FXML
    void mensaje_alimentar(ActionEvent event) {
        
    }



    @FXML
    void mensaje_limpiar(ActionEvent event) {
       
    }
    @FXML
    public void item_menu1(ActionEvent event) {
        

    }

    @FXML
    public void item_menu2(ActionEvent event) {
         
    }

    @FXML
    public void menu_mascotas(ActionEvent event) {

    }
    
    public void ingresa_hora(MouseEvent event) {
        
        
    }
    

    


    @Override
    public void initialize(URL location, ResourceBundle resources ){
        label_dia.setText("Día\n Lunes");
        label_hora.setText("Hora\n"+0+":00 hrs");
        barra_felicidad.setProgress(1.0);
        barra_salud.setProgress(1.0);
        barra_hambre.setProgress(0.0);
        barra_suciedad.setProgress(0.0);
        Nombre.setText("Nombre del Jugador: ");
        
        
    
    }

    
}
