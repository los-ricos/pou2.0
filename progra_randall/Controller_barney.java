package tamaguchi;

import com.gluonhq.charm.glisten.control.ExpansionPanel.ExpandedPanel;
import static java.lang.Thread.sleep;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import java.util.ArrayList;
import javafx.scene.input.TouchEvent;

public class Controller_barney implements Initializable   {
    
    @FXML
    private VBox BOX;

    @FXML
    private AnchorPane panel;

    @FXML
    private ExpandedPanel panel_extendido;

    @FXML
    private MenuButton menu_mascotas;

    @FXML
    private MenuItem item_menu1;

    @FXML
    private MenuItem item_menu2;


    @FXML
    private Label label_felicidad;

    @FXML
    private ProgressBar barra_felicidad;

    @FXML
    private Label label_salud;

    @FXML
    private ProgressBar barra_salud;

    @FXML
    private Label label_suciedad;

    @FXML
    private ProgressBar barra_suciedad;

    @FXML
    private Label label_hambre;

    @FXML
    private ProgressBar barra_hambre;

    @FXML
    private Button boton_curar;

    @FXML
    private ImageView imagen_curar;

    @FXML
    private Button boton_limpiar;

    @FXML
    private ImageView imagen_limpiar;

    @FXML
    private Button boton_acariciar;

    @FXML
    private ImageView imagen_acariciar;

    @FXML
    private Button boton_alimentar;

    @FXML
    private ImageView imagen_alimentar;

    @FXML
    private Separator separador_vertical;

    @FXML
    private Separator separador_horizontal;

    @FXML
    private Label label_dia;

    @FXML
    private Label label_hora;

    @FXML
    private Label label_sentimiento;

    @FXML
    private Label label_mascota;

    @FXML
    private ImageView imagen_dogge;
    
    @FXML
    private Label Puntaje;
    
    
    private String dia; 
    
    private int indice=0;
    private int hora=1;
    private boolean touch_button=false;
   
    
    public void comenzar_juego(ActionEvent event){


       
    }
  
    
    @FXML
    public void mensaje_acariciar(ActionEvent event) {

    }


    @FXML
    public void mensaje_curar(ActionEvent event) {


    }
   


    @FXML
    void mensaje_alimentar(ActionEvent event) {
    
    }



    @FXML
    void mensaje_limpiar(ActionEvent event) {
    
    }
    @FXML
    public void item_menu1(ActionEvent event) {
        
        
    
    }

    @FXML
    public void item_menu2(ActionEvent event) {

    }

    @FXML
    public void menu_mascotas(ActionEvent event) {

    }
    
    public void ingresa_hora(MouseEvent event) {
        
        
    }
    

    


    @Override
    public void initialize(URL location, ResourceBundle resources ){
        label_dia.setText("Día\n Lunes");
        label_hora.setText("Hora\n"+0+":00 hrs");
        barra_felicidad.setProgress(1.0);
        barra_salud.setProgress(1.0);
        barra_hambre.setProgress(0.0);
        barra_suciedad.setProgress(0.0);
        ReadJsonBARNEY readB= new ReadJsonBARNEY();
        readB.read();
        Mascota barney= new Mascota(boton_alimentar,boton_acariciar,boton_limpiar,boton_curar,label_hora,label_dia,barra_hambre,barra_suciedad,barra_salud,barra_felicidad,2,7,2,24,Puntaje);
        barney.iniciar_todo();
        
        
        
    
    }

    
}
