/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tamaguchi;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

/**
 *
 * @author rzumb
 */
public class hambre extends Task<Object>{
    
    private int hora_reloj=0;
    private boolean hora_atraso=false;
    private int atraso=2;
    private int hora_comer;
    private Button boton;
    
    
    private int subir=0;

    public hambre(int hora_comer, Button boton) {
        this.hora_comer = hora_comer;
        this.boton=boton;
    }
 
    public int mascota_hambre(){
        int result;
        boton.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event){
                
                updateProgress(0,10);
               
                subir=0;
                hora_atraso=false;
                atraso=2;
                
            }
        });
       
        if(hora_reloj==0){
            result=subir=0;
            updateProgress(subir,10);
            
        }
        
        if(hora_reloj==hora_comer){
            if(subir >=10){
               result=subir=10;
              updateProgress(subir,10);  
              
            return result;
            }
            else{
            result=subir=subir+2;
            
            updateProgress(subir,10);
            hora_atraso=true;
            
            return result;
            }
          
        }
        
        if(hora_reloj==hora_comer+atraso && hora_atraso==true){
            if(subir >=10){
               result=subir=10;
              updateProgress(subir,10);  
            
            return result;
            }
            else{
            result=subir=subir+2;
            updateProgress(subir,10);
            atraso=atraso+2;
            
            return result;
            }
        }
        if(hora_reloj == 24){
        
            hora_reloj=0;
            atraso=2;
        }
        result=subir;
        
        return result;
    
    }

    @Override
    protected Object call() throws Exception {
        while(true){
            Thread.sleep(5000);
            mascota_hambre();
            hora_reloj++;
            
        
        
        
        }
       
        
    }
}

    
    

